from apps.urls.models import Url
from apps.urls.serializers import UrlSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import BasePermission, IsAuthenticated, SAFE_METHODS
from .short_path import ShortPath


class UrlView(APIView):
    """
    List all urls, or create a new url.
    """
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        urls = Url.objects.filter(owner=request.user)
        serializer = UrlSerializer(urls, many=True)
        return Response(serializer.data)


    def post(self, request):
        request.data['owner'] = request.user.id
        serializer = UrlSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            saved_url = Url.objects.get(id=serializer.data["id"])
            saved_url.short_path = ShortPath().create_path(saved_url.id, saved_url.long_url, saved_url.short_path)
            saved_url.save()
            return Response(UrlSerializer(saved_url).data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
