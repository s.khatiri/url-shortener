from short_url import encode_url, decode_url

class ShortPath:


    def create_path(self, id: int, long_url: str, suggested_path: str = None) -> str:
        if suggested_path is None or len(suggested_path) == 0:
            return encode_url(id)
        else:
            return suggested_path + "_" + encode_url(id)

    def get_url_id(self, short_path: str) -> int:
        parts = short_path.split("_")
        if len(parts) > 1:
            return decode_url(parts[1])
        else:
            return decode_url(short_path)
