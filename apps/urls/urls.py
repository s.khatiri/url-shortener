from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from apps.urls import views

urlpatterns = [
    path('', views.UrlView.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)