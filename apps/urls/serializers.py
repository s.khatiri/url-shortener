from rest_framework import serializers
from apps.urls.models import Url
from django.contrib.auth.models import User


class UrlSerializer(serializers.ModelSerializer):

    class Meta:
        model = Url
        fields = '__all__'

