from django.db import models
from django.contrib.auth.models import User


class Url (models.Model):
    owner = models.ForeignKey(User, related_name='urls', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    long_url = models.CharField(max_length=1000)
    short_path = models.CharField(max_length=100, blank=True, null=True)


