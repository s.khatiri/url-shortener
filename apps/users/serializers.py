from rest_framework import serializers
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    #urls = serializers.HyperlinkedRelatedField(
    #    many=True, view_name='urls', read_only=True)

    class Meta:
        model = User
        fields = '__all__'
