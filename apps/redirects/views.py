from apps.redirects.models import Redirect
from rest_framework.views import APIView
from apps.urls.models import Url
from django.shortcuts import redirect
from django.http import Http404
from apps.urls.short_path import ShortPath


class RedirectToUrl(APIView):
    """
    Lookup short path and redirect to assigned url
    """
    def get(self, request, short_path: str):
        """
        looks up short path in stored urls and redirects request to correct url
        logs request details for further analytics
        returns 404 if no match is found
        :param request: http request
        :param short_path: short_path associated with url
        :return:
        """
        url = self._lookup_url(short_path)
        if url:
            self._log_redirect(request, url)
            return redirect(url.long_url)
        else:
            raise Http404

    def _lookup_url(self, short_path):
        #todo using appropriate caching for performacne
        try:
            url = Url.objects.get(id=ShortPath().get_url_id(short_path))
            return url
        except Url.DoesNotExist:
            return None

    def _log_redirect(self, request, url):
        log = Redirect()

        if request.user_agent.is_mobile:
            log.device = "mobile"
        elif request.user_agent.is_tablet:
            log.device = "tablet"
        elif request.user_agent.is_pc:
            log.device = "desktop"
        elif request.user_agent.is_bot:
            log.device = "bot"
        else:
            log.device = "other"

        log.browser = request.user_agent.browser.family

        log.ip = request.META.get('REMOTE_ADDR')

        log.url = url

        log.save()

