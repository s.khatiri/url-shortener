from django.db import models


class Redirect(models.Model):
    url = models.ForeignKey(
        'urls.url', related_name='redirects', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    ip = models.CharField(max_length=50)
    browser = models.CharField(max_length=50)
    device = models.CharField(max_length=50)



