from django.urls import path
from apps.redirects import views

urlpatterns = [
    path('<str:short_path>/', views.RedirectToUrl.as_view()),
]