from rest_framework import serializers
from apps.analytics.models import DailySummary


class AnalyticsSerializer(serializers.ModelSerializer):
    class Meta:
        model = DailySummary
        fields = ('category', 'count')
