from rest_framework.views import APIView
from rest_framework.response import Response
from apps.analytics.models import DailySummary, SummaryCategory, SummaryPeriod
from apps.analytics.serializers import AnalyticsSerializer


class AnalyticsView(APIView):
    """ Analytics View """

    def get (self, request, url_id, period: str, categories: str = None):

        distinct = bool(request.GET.get('distinct', False))
        period_days = SummaryPeriod[period]
        category_type = SummaryCategory[categories]
        analytics = DailySummary().load_analytics(url_id, period_days, category_type, distinct)
        serialized = AnalyticsSerializer(analytics, many=True)
        return Response(serialized.data)



