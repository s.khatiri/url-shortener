from django.contrib import admin
from apps.analytics.models import DailySummary

admin.site.register(DailySummary)
