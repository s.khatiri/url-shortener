from django.urls import path
from apps.analytics import views

urlpatterns = [
    path('<str:url_id>/<str:period>/<str:categories>/', views.AnalyticsView.as_view()),
    path('<str:url_id>/<str:period>/', views.AnalyticsView.as_view()),
]