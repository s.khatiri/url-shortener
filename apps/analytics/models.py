from django.db import models
from django.db.models import Count, Sum
from enum import Enum
from datetime import date,time,datetime,timedelta
from apps.redirects.models import Redirect


class SummaryPeriod(Enum):
    today = 0
    yesterday = 1
    week = 7
    month = 30


class SummaryCategory(Enum):
    none = 0
    device = 1
    browser = 2


class PeriodSummary(models.Model):
    url = models.ForeignKey(
        'urls.url', related_name='period_summaries', on_delete=models.CASCADE)
    date = models.DateField()
    period = models.IntegerField()
    category = models.CharField(max_length=100)
    category_type = models.IntegerField()
    count = models.IntegerField()
    distinct = models.BooleanField()

    @staticmethod
    def load_summaries(url_id: int, period: SummaryPeriod, categories: SummaryCategory, distinct: bool):
        if period!=SummaryPeriod.today:
            summaries = PeriodSummary.objects.filter(url_id=url_id
                                                     , period=period.value
                                                     , category_type=categories.value
                                                     , is_distinct=distinct
                                                     , date=date.today()
            return summaries
        else:


    @staticmethod
    def summarize_period(url_id: int, period: SummaryPeriod, categories: SummaryCategory, distinct: bool):
        daily_summaries = DailySummary.objects.filter(url_id=url_id
                                                      , category_type=categories.value
                                                      , is_distinct=distinct
                                                      , date__range=
                                                      [date.today()-timedelta(days=period), date.today()])
        category_counts = daily_summaries.values('category').annotate(count=Sum('count'))
        for category_count in category_counts:
            period_summary = PeriodSummary(url_id=url_id
                                           , period=period.value
                                           , date=date.today()
                                           , category=category_count['count']
                                           , category_type=categories.value
                                           , distinct=distinct
                                           , count=category_count['category'])
            period_summary.save()


class DailySummary(models.Model):
    url = models.ForeignKey(
        'urls.url', related_name='daily_summaries', on_delete=models.CASCADE)
    date = models.DateField()
    category = models.CharField(max_length=100)
    category_type = models.IntegerField()
    count = models.IntegerField()
    distinct = models.BooleanField()

    @staticmethod
    def analyze_today(url_id: int, for_date: datetime):


    @staticmethod
    def summarize_day(url_id: int, for_date:datetime):

        all_redirects = Redirect.objects.filter(url_id =url_id
                                                , created__range=[for_date.date()
                                                , for_date.date()+timedelta(days=1)])

        #total
        total=DailySummary(url_id=url_id,
                           date=for_date
                           , category_type=0
                           , category='all'
                           , distinct=False,
                           count=all_redirects.count()
                           )
        total.save()

        total_distinct=DailySummary(url_id=url_id,
                                    date=for_date
                                    , category_type=0
                                    , category='all',
                                    distinct=True,
                                    count=all_redirects.values('ip').distinct().count()
                                    )
        total_distinct.save()


        #device
        devices = all_redirects.values('device').annotate(count=Count('ip'))
        for device in devices:
            device_analytics = DailySummary(url_id=url_id,
                                            date=for_date
                                            , category_type=1
                                            , category=device['device'],
                                            distinct=False,
                                            count=device['count']
                                            )
            device_analytics.save()

        devices_distinct = all_redirects.values('device').annotate(count=Count('ip',distinct=True))
        for device in devices_distinct:
            device_analytics = DailySummary(url_id=url_id,
                                            date=for_date
                                            , category_type=1
                                            , category=device['device'],
                                            distinct=True,
                                            count=device['count']
                                            )
            device_analytics.save()

        #browser
        browsers = all_redirects.values('browser').annotate(count=Count('ip'))
        for browser in browsers:
            browser_analytics = DailySummary(url_id=url_id,
                                             date=for_date
                                             , category_type=2
                                             , category=browser['browser'],
                                             distinct=False,
                                             count=browser['count']
                                             )
            browser_analytics.save()

        browsers_distinct = all_redirects.values('browser').annotate(count=Count('ip',distinct=True))
        for browser in browsers_distinct :
            browser_analytics = DailySummary(url_id=url_id,
                                             date=for_date
                                             , category_type=2
                                             , category=browser['browser'],
                                             distinct=True,
                                             count=browser['count']
                                             )
            browser_analytics.save()

